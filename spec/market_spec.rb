# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/market'

RSpec.describe Market do
  context 'methods that set the price randomly' do
    let(:subject) { described_class.new }

    describe '#set_lemon_price' do
      it 'sets the lemon price randomly' do
        expect(subject.set_lemon_price).to be_between(25, 50)
      end
    end

    describe '#set_sugar_price' do
      it 'sets the sugar price radomly' do
        expect(subject.set_sugar_price).to be_between(2, 5)
      end
    end
  end
end
