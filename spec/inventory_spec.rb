require 'spec_helper'
require_relative '../lib/inventory'

RSpec.describe Inventory do
  context 'initialized with no arguments' do
    let(:input) { Hash.new }
    let(:subject) { described_class.new(**input) }

    it 'it starts with $5' do
      expect(subject.cash).to eq 5.00
    end

    it 'it starts with no lemons' do
      expect(subject.lemons).to eq 0
    end

    it 'it starts with no sugar' do
      expect(subject.sugar).to eq 0
    end

    it 'it starts with no lemonade' do
      expect(subject.lemonades).to eq 0
    end

    describe '#debit' do
      it 'returns the new amount' do
        expect(subject.debit(2.0)).to eq 3.0
      end

      it 'changes @cash' do
        subject.debit 3.0
        expect(subject.cash).to eq 2.0
      end

      it "raises an error when there isn't enough cash" do
        expect { subject.debit 10.0 }.to raise_error Inventory::Bankrupt
      end
    end

    describe '#credit' do
      it 'returns the new amount' do
        expect(subject.credit(5.0)).to eq 10.0
      end

      it 'changes @cash' do
        subject.credit 100.0
        expect(subject.cash).to eq 105.0
      end
    end

    describe '#take_lemons' do
      it 'returns the new amount' do
        subject.instance_variable_set :@lemons, 5
        expect(subject.take_lemons(2)).to eq 3
      end

      it 'changes @lemons' do
        subject.instance_variable_set :@lemons, 5
        subject.take_lemons 3
        expect(subject.lemons).to eq 2
      end

      it "raises an error when there aren't enough lemons" do
        expect { subject.take_lemons 2 }.to raise_error Inventory::NotEnough
      end
    end

    describe '#give_lemons' do
      it 'returns the new amount' do
        expect(subject.give_lemons(5)).to eq 5
      end

      it 'changes @lemons' do
        subject.give_lemons 5
        expect(subject.lemons).to eq 5
      end
    end

    describe '#take_sugar' do
      it 'returns the new amount' do
        subject.instance_variable_set :@sugar, 5
        expect(subject.take_sugar(2)).to eq 3
      end

      it 'changes @sugar' do
        subject.instance_variable_set :@sugar, 5
        subject.take_sugar 3
        expect(subject.sugar).to eq 2
      end

      it "raises an error when there aren't enough lemons" do
        expect { subject.take_sugar 2 }.to raise_error Inventory::NotEnough
      end
    end

    describe '#give_sugar' do
      it 'returns the new amount' do
        expect(subject.give_sugar(5)).to eq 5
      end

      it 'changes @lemons' do
        subject.give_sugar 5
        expect(subject.sugar).to eq 5
      end
    end

    describe '#make_lemonade' do
      it 'makes a lemonade out of one of each' do
        subject.instance_variable_set :@lemons, 1
        subject.instance_variable_set :@sugar, 1

        expect(subject.make_lemonade).to eq 1
        expect(subject.lemons).to eq 0
        expect(subject.sugar).to eq 0
      end

      it "raises an error if there aren't enough lemons or sugar" do
        expect { subject.make_lemonade }.to raise_error Inventory::NotEnough
      end

      it 'changes @lemonades' do
        subject.instance_variable_set :@lemons, 1
        subject.instance_variable_set :@sugar, 1

        subject.make_lemonade
        expect(subject.lemonades).to eq 1
      end

      it 'makes as many lemonades as possible, leaving the remaining lemons' do
        subject.instance_variable_set :@lemons, 50
        subject.instance_variable_set :@sugar, 40

        expect(subject.make_lemonade(all: true)).to eq 40
        expect(subject.lemons).to eq 10
        expect(subject.sugar).to eq 0
      end

      it 'makes as many lemonades as possible, leaving the remaining sugar' do
        subject.instance_variable_set :@lemons, 40
        subject.instance_variable_set :@sugar, 45

        expect(subject.make_lemonade(all: true)).to eq 40
        expect(subject.lemons).to eq 0
        expect(subject.sugar).to eq 5
      end
    end

    describe '#take_lemonade' do
      it 'returns the new amount' do
        subject.instance_variable_set :@lemonades, 5
        expect(subject.take_lemonade(2)).to eq 3
      end

      it 'changes @lemonades' do
        subject.instance_variable_set :@lemonades, 5
        subject.take_lemonade 3
        expect(subject.lemonades).to eq 2
      end

      it "raises an error when there aren't enough lemonades" do
        expect { subject.take_lemonade 2 }.to raise_error Inventory::NotEnough
      end
    end
  end

  context 'initialized with partial inventory' do
    let(:input) { { lemons: 4, sugar: 2 } }

    it 'has the right amount of lemons' do
      expect(subject.lemons).to eq 4
    end

    it 'has the right amount of sugar' do
      expect(subject.sugar).to eq 2
    end

    it 'defaults to the right amount of lemonade' do
      expect(subject.lemonade).to eq 0
    end

    it 'defaults to the right amount of cash' do
      expect(subject.cash).to eq 5.0
    end
  end

  context 'initialized with complete inventory' do
    let(:input) { { cash: 1_000.0, lemons: 4, sugar: 2, lemonades: 1 } }

    it 'has the right amount of lemons' do
      expect(subject.lemons).to eq 4
    end

    it 'has the right amount of sugar' do
      expect(subject.sugar).to eq 2
    end

    it 'has the right amount of lemonade' do
      expect(subject.lemonade).to eq 1
    end

    it 'has the right amount of lemonade' do
      expect(subject.cash).to eq 1_000.0
    end

    describe '#take_lemons' do
      it 'returns the new amount' do
        subject.instance_variable_set :@lemons, 5
        expect(subject.take_lemons(2)).to eq 3
      end

      it 'changes @lemons' do
        subject.instance_variable_set :@lemons, 5
        subject.debit 3
        expect(subject.cash).to eq 2
      end

      it "raises an error when there isn't enough cash" do
        expect { subject.debit 10.0 }.to raise_error Inventory::Bankrupt
      end
    end

    describe '#give_lemons' do
      # give lemons
    end

    describe '#take_sugar' do
      # take away sugar
    end

    describe '#give_sugar' do
      # give sugar
    end
  end
end
