# frozen_string_literals: true

# Represents the player's inventory.
class Inventory
  attr_reader :cash, :lemons, :sugar, :lemonades

  def initialize(cash: 5.0, lemons: 0, sugar: 0, lemonades: 0)
    @cash = cash
    @lemons = lemons
    @sugar = sugar
    @lemonades = lemonades
  end

  def debit(amount)
    raise Bankrupt if @cash < amount

    @cash -= amount
  end

  def credit(amount)
    @cash += amount
  end

  def take_lemons(amount)
    raise NotEnough if @lemons < amount

    @lemons -= amount
  end

  def give_lemons(amount)
    @lemons += amount
  end

  def take_sugar(amount)
    raise NotEnough if @sugar < amount

    @sugar -= amount
  end

  def give_sugar(amount)
    @sugar += amount
  end

  def make_lemonade(all: false)
    count = all && [@lemons, @sugar].min || 1

    take_lemons count
    take_sugar count

    @lemonades += count
  end

  def take_lemonade(amount)
    raise NotEnough if @lemonades < amount

    @lemonades -= amount
  end

  # Raised when there the debit request would bankrupt the business.
  class Bankrupt < StandardError; end

  # Raised when there aren't enough items in the inventory.
  class NotEnough < StandardError; end
end
