# frozen_string_literal: true

# Class for calculating lemon and sugar prices
class Market
  def set_lemon_price
    rand(25..50)
  end

  def set_sugar_price
    rand(2..5)
  end
end
